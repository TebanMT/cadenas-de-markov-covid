# Evolución de Pacientes Positivos a COVID-19 en el Estado de Guanajuato Usando Cadenas de Markov en Tiempo Discreto

En este trabajo se ha realizado un modelo de una cadena de Markov en tiempo discreto utilizando datos públicos de pacientes que han experimentado sı́ntomas asociados con la enfermedad COVID-19. Los datos publicados por el gobierno federal de México se utilizó para ajustar el modelo con siete diferentes estados. El modelo proporciona información para evaluar el riesgo de fallecimiento o recuperación de personas positivas al virus SARS-COV-2 en el estado de Guanajuato.

## Construido con 🛠️


* [R]- Implementacion de la cadena de Márkov
* [Python] - Manipulación de datos

## Estado

#### Finalizado con articulo (no publicado)
